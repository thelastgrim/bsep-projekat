import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CertificateService } from 'src/services/certificate-service/certificate.service';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  constructor(private router:Router, private certificateService: CertificateService) { }

  alias: String

  ngOnInit(): void {
  }
  
  create(){
    this.router.navigateByUrl("/createCertificate")
  }

  list(){
    this.router.navigateByUrl("/listCertificate")
  }

  check(){
    
    this.certificateService.checkValidity(this.alias).subscribe((data) => {
      alert(data.body);
    });
  }
}

