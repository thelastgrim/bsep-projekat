import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CertificateService } from 'src/services/certificate-service/certificate.service';
import { Certificate } from '../model/certificate';

@Component({
  selector: 'app-list-certificates',
  templateUrl: './list-certificates.component.html',
  styleUrls: ['./list-certificates.component.scss'],
})
export class ListCertificatesComponent implements OnInit {
  elements: any = [];
  certReasons: any = [];
  certReason : string ;
  headElements = [
    'Naziv sertifikata',
    'Ime',
    'Prezime',
    'Vazi od:',
    'Vazi do:',
    'Povuci sertifikat',
  ];
  certificates!: Certificate[];

  constructor(
    private certificateService: CertificateService,
    private router: Router
  ) {}

  ngOnInit(): void {
    /*  for (let i = 1; i <= 15; i++) {
      this.elements.push({
        id: i, first: 'User ' + i, last: 'Name ' + i, handle:
          'Handle ' + i
      });
    }
    */
    this.certificateService.getAll().subscribe((data) => {
      console.log('GEt all certs');
      console.log(data.body);
      this.certificates = data.body;
    });

    this.certificateService.getRevokeReason().subscribe((data) => {
      console.log(data.body);
      this.certReasons = data.body;
      // (document.querySelector('.dropdown-menu') as HTMLElement).style.display =
      //   'block';
    });
  }
  selectChangeHandler(event : any){
    this.certReason = event.target.value;
    console.log(this.certReason);
  }

  // revokeCertificate(cert) {
  //   this.certificateService.revokeCertificate(
  //     new Certificate({ certificateName: cert })
  //   );
  // }
  revokeCertificate(certificate : Certificate) {
    console.log(this.certReason);
    this.certificateService.revokeCertificate({subjectAlias: certificate.certificateName, revokeReason: this.certReason}).toPromise().then(() => {
      alert("Certificate successufully revoked!");
    });

  }
}
