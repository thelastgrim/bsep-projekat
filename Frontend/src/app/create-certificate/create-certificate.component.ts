import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CertificateService } from 'src/services/certificate-service/certificate.service';
import { Certificate } from '../model/certificate';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-create-certificate',
  templateUrl: './create-certificate.component.html',
  styleUrls: ['./create-certificate.component.scss']
})
export class CreateCertificateComponent implements OnInit {

  constructor(private certificateService:CertificateService,private router:Router) { }
  certificate: Certificate = new Certificate;
  isShown: boolean = false
  model : NgbModule
  model0 : NgbModule
  val: any
  val0: any
  selected: String

  certificateRequests!: String[];

  certificates!: Certificate[];


  ngOnInit(): void {
    this.certificateService.getCertificateRequests().subscribe((data) => {
      console.log(data.body)
      this.certificateRequests = data.body;
    });

    this.certificateService.getAll().subscribe((data) => {
      console.log('GEt all certs');
      console.log(data.body);
      this.certificates = data.body;
    });

  }
  

  onChange(value: string): void {
    console.log("Usli smo u on change:")
    console.log(value)
    if(value == ""){
      this.isShown = true;
    }else{
      this.isShown = false
    }
  }

  onChangeDate(): void {
    console.log("Usli smo u on change date:")
    this.isShown = false;
    // console.log(value)
    // if(value == ""){
    //   this.isShown = true;
    // }else{
    //   this.isShown = false
    // }
  }
  create(){
        console.log("Usli smo u create: ")
        console.log(this.selected)
        console.log("Nako")
        console.log(this.certificate)
        if(this.model == undefined ||this.model0 == undefined){
          console.log("MOdel je undefined")
          this.isShown = true;
        }else{
          this.val = this.model
          this.val0 = this.model0

          this.certificate.endDate = this.val.year
          this.certificate.startDate = this.val0.year

          if(Number(this.val.month) <10){ 
           this.certificate.endDate += "-0" + this.val.month 
          }else{
            
            this.certificate.endDate += "-" + this.val.month 
          }

          if(Number(this.val.day) <10){ 
            this.certificate.endDate += "-0" + this.val.day 
          }else{
             
           this.certificate.endDate +=   "-" + this.val.day 
          }



           if(Number(this.val0.month) <10){ 
            this.certificate.startDate += "-0" + this.val0.month 
           }else{
             
           this.certificate.startDate +=  "-" + this.val0.month 
           }
 
           if(Number(this.val0.day) <10){ 
             this.certificate.startDate += "-0" + this.val0.day 
            }else{
              
            this.certificate.startDate +=  "-" +  this.val0.day 
            }
          
          if(this.certificate.certificateName ==null ||this.certificate.certificateName =="" || this.certificate.country ==null ||this.certificate.country =="" || this.certificate.email ==null ||this.certificate.email =="" || this.certificate.endDate ==null ||this.certificate.endDate ==""|| this.certificate.startDate ==null ||this.certificate.startDate ==""|| this.certificate.givenName ==null ||this.certificate.givenName ==""|| this.certificate.organization ==null ||this.certificate.organization ==""|| this.certificate.organizationalUnit ==null ||this.certificate.organizationalUnit ==""|| this.certificate.surname ==null ||this.certificate.surname ==""){
            this.isShown = true;
          }else{
            // console.log(this.model)
            // this.val = this.model
            // console.log("Val day")
            // console.log(this.val.day)


           // this.certificate.startDate = "2020-05-05"
          //  this.certificate.endDate = "2021-05-05"
          console.log("Pre slanja zahteva " )
          console.log(this.certificate)
            this.certificateService.create(this.certificate).subscribe((data) =>{
              alert("Sertifikat je uspesno kreiran")
              this.router.navigateByUrl("/content")
            });
          
          }
      }

 
  }

}