import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { CreateCertificateComponent } from './create-certificate/create-certificate.component';
import { ListCertificatesComponent } from './list-certificates/list-certificates.component';


const routes: Routes = [
  { path: '',   redirectTo: 'content', pathMatch: 'full' },
  { path: 'content', component: ContentComponent},
  { path: 'createCertificate', component: CreateCertificateComponent},
  { path: 'listCertificate', component: ListCertificatesComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
