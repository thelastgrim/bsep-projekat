export class Certificate {
  root: string = '';
  email: string = '';
  certificateName: string = '';
  surname: string = '';
  givenName: string = '';
  organization: string = '';
  organizationalUnit: string = '';
  country: string = '';
  startDate: string = '';
  endDate: string = '';
  revokeReason : string;
  request : string

  constructor(obj?: any) {
    this.root = (obj && obj.root) || null;
    this.email = (obj && obj.email) || null;
    this.certificateName = (obj && obj.certificateName) || null;
    this.surname = (obj && obj.surname) || null;
    this.givenName = (obj && obj.givenName) || null;
    this.organization = (obj && obj.organization) || null;
    this.organizationalUnit = (obj && obj.organizationalUnit) || null;
    this.country = (obj && obj.country) || null;
    this.startDate = (obj && obj.startDate) || null;
    this.endDate = (obj && obj.endDate) || null;
    this.revokeReason = (obj && obj.revokeReason) || null;
    this.request = (obj && obj.request) || null;
  }
}
