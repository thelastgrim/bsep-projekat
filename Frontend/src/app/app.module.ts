import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import {MatButtonModule} from '@angular/material/button';
import { ContentComponent } from './content/content.component';
import { CreateCertificateComponent } from './create-certificate/create-certificate.component';
import { FormsModule } from '@angular/forms';
//import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap/datepicker/datepicker.module';
import { NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ListCertificatesComponent } from './list-certificates/list-certificates.component';


@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    CreateCertificateComponent,
    ListCertificatesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    NgbModule,
    FormsModule
    
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
