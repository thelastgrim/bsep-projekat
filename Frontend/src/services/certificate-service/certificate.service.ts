import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Certificate } from 'src/app/model/certificate';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CertificateService {
  private readonly path = 'https://localhost:8443/';

  constructor(private http: HttpClient) {}

  create(certificate: Certificate): Observable<HttpResponse<any>> {
    return this.http
      .post<Certificate>(this.path + 'certificates', certificate, {
        observe: 'response',
      })
      .pipe(
        catchError((err) => {
          console.log(err.error);
          console.log(err.error.message);
          alert('Greska na serveru');
          return throwError(err);
        })
      );
  }

  getAll(): Observable<HttpResponse<any>> {
    return this.http
      .get<Certificate[]>(this.path + 'certificates', { observe: 'response' })
      .pipe(
        catchError((err) => {
          console.log(err.error);
          console.log(err.error.message);
          alert('Greska na serveru');
          return throwError(err);
        })
      );
  }

  revokeCertificate(revokation: {
    subjectAlias: string;
    revokeReason: string;
  }){
    return this.http.post<any>(this.path + 'certificates/revoke', revokation);
  }

  getRevokeReason(): Observable<HttpResponse<any>> {
    return this.http.get(this.path + 'certificates/getRevokeReason', {
      observe: 'response',
    });
  }

  checkValidity(alias: String): Observable<HttpResponse<any>> {
   
    return this.http
      .post<any>(this.path + 'certificates/check', alias, {
        observe: 'response',
      })
  }

  getCertificateRequests(): Observable<HttpResponse<any>> {
    return this.http
      .get<any>(this.path + 'certificates/loads', { observe: 'response' })
      .pipe(
        catchError((err) => {
          console.log(err.error);
          console.log(err.error.message);
          alert('Greska na serveru');
          return throwError(err);
        })
      );
  }
}
