package com.securityWebApi.exceptions;

public class CertificateNotFoundEx extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CertificateNotFoundEx() {
		super("Certificate not found");
	}

}
