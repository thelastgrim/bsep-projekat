package com.securityWebApi.DTO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.securityWebApi.enums.RevokeReason;

public class RevokeCertificateDTO {
	
	@NotNull
	@NotBlank
	private String revokeReason;
	@NotNull
	@NotBlank
	private String subjectAlias;
	
	public RevokeCertificateDTO(@NotNull @NotBlank String revokeReason, @NotNull @NotBlank String subjectAlias) {
		super();
		this.revokeReason = revokeReason;
		this.subjectAlias = subjectAlias;
	}
	public RevokeCertificateDTO() {super();}
	
	public String getRevokeReason() {
		return revokeReason;
	}

	public void setRevokeReason(String revokeReason) {
		this.revokeReason = revokeReason;
	}

	public String getSubjectAlias() {
		return subjectAlias;
	}

	public void setSubjectAlias(String subjectAlias) {
		this.subjectAlias = subjectAlias;
	}

	@Override
	public String toString() {
		return "RevokeCertificateDTO{" +
				"revokeReason='" + revokeReason + '\'' +
				", subjectAlias='" + subjectAlias + '\'' +
				'}';
	}
}
