package com.securityWebApi.DTO;

import java.util.Date;

public class DTOResponseCertificate {
	
	private String certificateName;
	private String surname;
	private String givenName;
	private Date startDate;
	private Date endDate;
	
	public DTOResponseCertificate() {
		super();
	}
	public DTOResponseCertificate(String certificateName, String surname, String givenName, Date startDate,
			Date endDate) {
		super();
		this.certificateName = certificateName;
		this.surname = surname;
		this.givenName = givenName;
		this.startDate = startDate;
		this.endDate = endDate;
	}	

	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
