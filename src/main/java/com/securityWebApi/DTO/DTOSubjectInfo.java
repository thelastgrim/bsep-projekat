package com.securityWebApi.DTO;

import java.util.Date;

public class DTOSubjectInfo {

	private String certificateName; 	//"Marija kovacevic"
	private String surname;				//"Kovacevic"
	private String givenName;			//"Marija"
	private String organization;		//"UNS-FTN"
	private String organizationalUnit;  //"Katedra za informatiku"
	private String country;				//"RS"
	private String email;				//"marija.kovacevic@uns.ac.rs"
	
	private Date startDate;
	private Date endDate;
	private String root;
	private String request;
	
	public DTOSubjectInfo(String request, String root, String certificateName, String surname, String givenName, String organization,
			String organizationalUnit, String country, String email, Date startDate, Date endDate) {
		super();
		this.request = request;
		this.root = root;
		this.certificateName = certificateName;
		this.surname = surname;
		this.givenName = givenName;
		this.organization = organization;
		this.organizationalUnit = organizationalUnit;
		this.country = country;
		this.email = email;
		this.startDate = startDate;
		this.endDate = endDate;
	}



	public DTOSubjectInfo() {
		super();
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getOrganizationalUnit() {
		return organizationalUnit;
	}

	public void setOrganizationalUnit(String organizationalUnit) {
		this.organizationalUnit = organizationalUnit;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public Date getStartDate() {
		return startDate;
	}



	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}



	public Date getEndDate() {
		return endDate;
	}



	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getRoot() {
		return root;
	}



	public void setRoot(String root) {
		this.root = root;
	}



	public String getRequest() {
		return request;
	}



	public void setRequest(String request) {
		this.request = request;
	}


}
