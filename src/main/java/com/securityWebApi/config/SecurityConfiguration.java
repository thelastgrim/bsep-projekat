package com.securityWebApi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	// Handler za vracanje 401 kada klijent sa neodogovarajucim korisnickim imenom i lozinkom pokusa da pristupi resursu
   
	 // Generalna bezbednost aplikacije
    @Override
    public void configure(HttpSecurity http) throws Exception {
        // TokenAuthenticationFilter ce ignorisati sve ispod navedene putanje
      //  web.ignoring().antMatchers(HttpMethod.POST, "/certificates");
      //  web.ignoring().antMatchers(HttpMethod.GET,"/", "/webjars/**", "/*.html", "/favicon.ico", "/**/*.html",
      //          "/**/*.css", "/**/*.js");
    	
    	 http
         // komunikacija izmedju klijenta i servera je stateless posto je u pitanju REST aplikacija
         .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

      
//         // svim korisnicima dopusti da pristupe putanji /auth/**
//         .authorizeRequests().antMatchers("/auth/**").permitAll()
         .authorizeRequests().antMatchers(
         		HttpMethod.GET,
         		"/", "/webjars/**", "/*.html", "/favicon.ico", "/**/*.html",
         	                "/**/*.css", "/**/*.js","/certificates","/certificates/*"
         		).permitAll()
         
         			.antMatchers(
         		HttpMethod.POST,
         		 "/certificates",
                  "/certificates/revoke",
                  "/certificates/check"
         		).permitAll()
        
         // umesto anotacija iynad svake metode, moze i ovde da se proveravaju prava pristupa ya odredjeni URL
         //.antMatchers(HttpMethod.GET, "/api/cultural-content-category").hasRole("ROLE_ADMIN")

         // za svaki drugi zahtev korisnik mora biti autentifikovan
         .anyRequest().authenticated().and()
         // za development svrhe ukljuci konfiguraciju za CORS iz WebConfig klase
         .cors().and()

        
 
         .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET")).logoutSuccessUrl("/").permitAll();
			//.logoutUrl("/logout");//.logoutSuccessUrl("/logout-success");

		 // zbog jednostavnosti primera
		 http.csrf().disable();
    }

}
