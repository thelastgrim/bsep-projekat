//package com.securityWebApi.initializeFiles;
//
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.security.KeyPair;
//import java.security.PrivateKey;
//import java.security.cert.CRLException;
//import java.security.cert.X509CRL;
//import java.security.cert.X509Certificate;
//import java.util.Date;
//import java.util.logging.Logger;
//
//import javax.annotation.PostConstruct;
//
//import org.apache.tomcat.util.bcel.Const;
//import org.bouncycastle.asn1.x500.X500Name;
//import org.bouncycastle.cert.X509CRLHolder;
//import org.bouncycastle.cert.X509v2CRLBuilder;
//import org.bouncycastle.cert.jcajce.JcaX509CRLConverter;
//import org.bouncycastle.operator.OperatorCreationException;
//import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.securityWebApi.certificates.CertificateGenerator;
//import com.securityWebApi.keystores.KeyStoreReader;
//import com.securityWebApi.keystores.KeyStoreWriter;
//import com.securityWebApi.model.IssuerBean;
//import com.securityWebApi.util.Constants;
//import com.securityWebApi.util.UtilSecurity;
//
//import jdk.internal.org.jline.utils.Log;
//
//@Component
//public class Init {
//	
//	KeyStoreReader keyStoreReader;
//	KeyStoreWriter keyStoreWriter;
//	
//	private static final Logger LOG = Logger.getLogger(Init.class.getName());
//	
//	@PostConstruct
//	public void init() {
//		LOG.info("Kreiranje KeyStora...");
//		createFiles();
//		
//	}
//	
//	public void createFiles() {
//		try {
//			createCer();
//		}catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
//		
//	}
//	
//	private void createCer() {
//		KeyStoreWriter keyStoreWriter = new KeyStoreWriter();
//		
//		System.out.println("Kreiranje KeyStora...");
//		keyStoreWriter.loadKeyStore(null, Constants.KEY_STORE_PASSWORD);
//		
//		UtilSecurity us = new UtilSecurity();
//		
//		KeyPair kpAdmin = us.generateKeyPair();
//		IssuerBean issuerData = us.getIssuerData(kpAdmin.getPrivate());
//		
//		CertificateGenerator cg = new CertificateGenerator();
//        X509Certificate cert = cg.generateCertificate(null, issuerData, kpAdmin.getPublic());
//        keyStoreWriter.write(Constants.ALIAS_SUPERADMIN, kpAdmin.getPrivate(),Constants.PASSWORD_SUPERADMIN,cert);
//        keyStoreWriter.saveKeyStore(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
//		System.out.println("Kreiran SUPER ADMIN certifikat i upisan u keystore.");
//	}
//	
//	private void createCRL(PrivateKey privateKey,X500Name issuerName) throws OperatorCreationException, CRLException, IOException {
//		X509v2CRLBuilder builder = new X509v2CRLBuilder(issuerName, new Date());
//		builder.setNextUpdate(new Date(System.currentTimeMillis() + 86400 * 1000));
//		
//		JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder("SHA256WithRSA");
//        contentSignerBuilder.setProvider("BC");
//
//        X509CRLHolder crlHolder = builder.build(contentSignerBuilder.build(privateKey));
//        JcaX509CRLConverter converter = new JcaX509CRLConverter();
//        converter.setProvider("BC");
//
//        X509CRL crl = converter.getCRL(crlHolder);
//
//        byte[] bytes = crl.getEncoded();
//
//
//        OutputStream os = new FileOutputStream("src/main/resources/certificationInfoList.crl");
//        os.write(bytes);
//        os.close();	
//		
//	}
//	
//}
