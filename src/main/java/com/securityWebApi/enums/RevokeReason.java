package com.securityWebApi.enums;

public enum RevokeReason {
	AA_COMPROMISE,
	//This reason indicates that it is known or suspected that the certificate subject's private key has been compromised.
	AFFILIATION_CHANGED,
	//This reason indicates that the subject's name or other information has changed.
	CA_COMPROMISE,
	//This reason indicates that it is known or suspected that the certificate subject's private key has been compromised.
	CERTIFICATE_HOLD,
	//This reason indicates that the certificate has been put on hold.
	CESSATION_OF_OPERATION,
	//This reason indicates that the certificate is no longer needed.
	KEY_COMPROMISE,
	//This reason indicates that it is known or suspected that the certificate subject's private key has been compromised.
	PRIVILEGE_WITHDRAWN,
	//This reason indicates that the privileges granted to the subject of the certificate have been withdrawn.
	REMOVE_FROM_CRL,
	//This reason indicates that the certificate was previously on hold and should be removed from the CRL.
	SUPERSEDED,
	//This reason indicates that the certificate has been superseded.
	UNSPECIFIED,
	//This reason indicates that it is unspecified as to why the certificate has been revoked.
	UNUSED
	//Unused reason.

}
