package com.securityWebApi.enums;

public enum CertificateStatus{
	NOT_EXISTING,
	EXPIRED,
	NOT_YET_VALID,
	REVOKED,
	PARENT_INVALID,
	VALID
}