package com.securityWebApi.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v2CRLBuilder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CRLConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jcajce.provider.asymmetric.X509;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.securityWebApi.DTO.DTOSubjectInfo;
import com.securityWebApi.DTO.RevokeCertificateDTO;
import com.securityWebApi.enums.CertificateStatus;
import com.securityWebApi.enums.RevokeReason;
import com.securityWebApi.keystores.KeyStoreReader;
import com.securityWebApi.model.IssuerBean;
import com.securityWebApi.model.SubjectBean;
import com.securityWebApi.util.Constants;
import com.securityWebApi.util.UtilSecurity;

@Service
public class CertificateService {
	
	@Autowired
	private UtilSecurity utilSecurity;
	
	private KeyStoreReader keyStoreRedear = new KeyStoreReader();

	public X509Certificate generateCertificate(IssuerBean issuerBean, SubjectBean subjectBean) {
		try {
			JcaContentSignerBuilder builder = new JcaContentSignerBuilder("SHA256WithRSAEncryption");
			builder = builder.setProvider("BC");
			
			ContentSigner contentSigner = builder.build(issuerBean.getPrivateKey());
			
			X509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(issuerBean.getX500name(),
					new BigInteger(subjectBean.getSerialNumber()),
					subjectBean.getStartDate(),
					subjectBean.getEndDate(),
					subjectBean.getX500name(),
					subjectBean.getPublicKey());
			
			X509CertificateHolder certHolder = certBuilder.build(contentSigner);
			JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
            certConverter = certConverter.setProvider("BC");
            return certConverter.getCertificate(certHolder);
			
		} catch (OperatorCreationException | CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return null;
	}
	
	public SubjectBean generateSubjectData(DTOSubjectInfo dtoSI, PublicKey publicKey) {
		SecureRandom random = new SecureRandom();
		
        try {
    		SimpleDateFormat iso8601Formater = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = iso8601Formater.parse(iso8601Formater.format(dtoSI.getStartDate()));
			Date endDate = iso8601Formater.parse(iso8601Formater.format(dtoSI.getEndDate()));
			
			String sn = String.valueOf(random.nextLong());
			X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
            builder.addRDN(BCStyle.CN, dtoSI.getCertificateName());
            builder.addRDN(BCStyle.SURNAME, dtoSI.getSurname());
            builder.addRDN(BCStyle.GIVENNAME, dtoSI.getGivenName());
            builder.addRDN(BCStyle.O, dtoSI.getOrganization());
            builder.addRDN(BCStyle.OU, dtoSI.getOrganizationalUnit());
            builder.addRDN(BCStyle.C, dtoSI.getCountry());
            builder.addRDN(BCStyle.E, dtoSI.getEmail());
            
            String uid = String.valueOf(Math.abs(random.nextInt()));
            builder.addRDN(BCStyle.UID, uid);
            System.out.println(sn);
            System.out.println(uid);
            
            return new SubjectBean(publicKey, builder.build(), sn, startDate, endDate);
            
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
		return null;	
	}

	public Certificate getCertificateByName(String name) throws IOException {
		
		File f = new File("src/main/resources/certificationInfoList.crl");
		byte[] bytes = Files.readAllBytes(f.toPath());
		X509CRLHolder holder = new X509CRLHolder(bytes);
		
		
		List<Certificate> certificates = keyStoreRedear.readAllCertificates(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
		
		for (Certificate certificate : certificates) {
			X509Certificate c = (X509Certificate) certificate;
			try {
				X500Name x500name = new JcaX509CertificateHolder(c).getSubject();
				//System.out.println("-----" + x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString());
				if(x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString().equals(name) && (holder.getRevokedCertificate(c.getSerialNumber()) == null)    ){
					return certificate;
				}

			} catch (CertificateEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
		}
		return null;
	}
	
	public void revokeCertificate(RevokeCertificateDTO revokeCerDTO) throws IOException, CertificateEncodingException, OperatorCreationException, CRLException {
		File f = new File("src/main/resources/certificationInfoList.crl");
		byte[] bytes = Files.readAllBytes(f.toPath());
		X509CRLHolder holder = new X509CRLHolder(bytes);
		X509v2CRLBuilder builder = new X509v2CRLBuilder(holder);
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

		Certificate cert = keyStoreRedear.readCertificate(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD,revokeCerDTO.getSubjectAlias());
		X509Certificate c = (X509Certificate) cert;
		System.out.println(c);
		X500Name x500name = new JcaX509CertificateHolder(c).getSubject();
		String password = x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString() + x500name.getRDNs(BCStyle.C)[0].getFirst().getValue().toString();

		Certificate certificate = keyStoreRedear.readCertificate(Constants.KEY_STORE_NAME,Constants.KEY_STORE_PASSWORD,revokeCerDTO.getSubjectAlias());
		JcaX509CertificateHolder cerHolder = new JcaX509CertificateHolder((X509Certificate) certificate);
		
		builder.addCRLEntry(cerHolder.getSerialNumber(),new Date(),RevokeReason.valueOf(revokeCerDTO.getRevokeReason()).ordinal());
		
		JcaContentSignerBuilder jcsb = new JcaContentSignerBuilder("SHA256WithRSA");
		jcsb.setProvider("BC");

		IssuerBean issuer = keyStoreRedear.readIssuerFromStore(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD, revokeCerDTO.getSubjectAlias(),  password);
		
		X509CRLHolder crlHolder = builder.build(jcsb.build(issuer.getPrivateKey()));
		JcaX509CRLConverter converter = new JcaX509CRLConverter();
		converter.setProvider("BC");
		
		X509CRL xcrl = converter.getCRL(crlHolder);
		bytes = xcrl.getEncoded();
		
		OutputStream outStream = new FileOutputStream("src/main/resources/certificationInfoList.crl");
		outStream.write(bytes);
		outStream.close();
		
		System.out.println(certificate.getPublicKey());
	}
	
	public List<RevokeReason> getRevokeReason()  {
		return Arrays.asList(RevokeReason.values());
	}
	
	
	private boolean checkIfChainValid(String alias) throws IOException, CertificateEncodingException {
		if(alias.equals("Goran Sladic")) {
			return false;
		}
		
		Certificate c = getCertificateByName(alias);
		
		X509Certificate xc = (X509Certificate) c;
		
		X500Principal pp = xc.getIssuerX500Principal();
		
		
		if(checkCertValidity(getCN(pp.getName())) == CertificateStatus.VALID) {
			return false;
		}
		
		return true;
		

	}

	

	public boolean checkIfExists(String alias) {
		List<Certificate> certificates = keyStoreRedear.readAllCertificates(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
		
		boolean exists = false;
		for (Certificate certificate : certificates) {
			X509Certificate c = (X509Certificate) certificate;
			try {
				X500Name x500name = new JcaX509CertificateHolder(c).getSubject();
				String existingAlies = x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString();
				if(existingAlies.equals(alias)) {
					exists = true;
				}
			
			} catch (CertificateEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return exists;
	}
	
	public int checkIfExpired(String alias) {
		List<Certificate> certificates = keyStoreRedear.readAllCertificates(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
		
		
		for (Certificate certificate : certificates) {
			X509Certificate c = (X509Certificate) certificate;
			
			try {
				X500Name x500name = new JcaX509CertificateHolder(c).getSubject();
				String existingAlies = x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString();
				if(existingAlies.equals(alias)) {
					try {
						c.checkValidity();
						return 0;
					}catch (CertificateExpiredException e) {
						return 1;
					}
					catch (CertificateNotYetValidException e) {
						return 2;
					}
				}
			
			} catch (CertificateEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		return 0;
		
		
	}
	
	public boolean checkIfRevoked(String alias) throws IOException {
		List<Certificate> certificates = keyStoreRedear.readAllCertificates(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
		File f = new File("src/main/resources/certificationInfoList.crl");
		byte[] bytes = Files.readAllBytes(f.toPath());
		X509CRLHolder holder = new X509CRLHolder(bytes);
		
		for (Certificate certificate : certificates) {
			X509Certificate c = (X509Certificate) certificate;
			
			try {
				X500Name x500name = new JcaX509CertificateHolder(c).getSubject();
				String existingAlies = x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString();
				if(existingAlies.equals(alias)) {
					if (holder.getRevokedCertificate(c.getSerialNumber()) != null) {
						return true;
					}else {
						return false;
					}
				}
			
			} catch (CertificateEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		return false;
		
		
	}


	
	public CertificateStatus checkCertValidity(String alias) throws IOException, CertificateEncodingException {
		
		int dateStatus = checkIfExpired(alias);
		if(dateStatus==1) {
			return CertificateStatus.EXPIRED;
		}
		else if (dateStatus==2) {
			return CertificateStatus.NOT_YET_VALID;
		}
			
		else if(checkIfRevoked(alias)) {
			return CertificateStatus.REVOKED;
		}
		
		else if(!checkIfExists(alias)) {
			return CertificateStatus.NOT_EXISTING;
		}
		else if(checkIfChainValid(alias)) {
			return CertificateStatus.PARENT_INVALID;
		}
		
		else {
			return CertificateStatus.VALID;
		}
	}
	
	
	private String getCN(String x509principal) {
		String[] items = x509principal.split(",");
		for (String string : items) {
			String[] singleItem = string.split("=");
			if(singleItem[0].equals("CN")) {
				return singleItem[1];
			}
		}
		
		return "";
	}
}
