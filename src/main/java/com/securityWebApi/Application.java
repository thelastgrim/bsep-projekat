package com.securityWebApi;

import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;

import com.securityWebApi.enums.RevokeReason;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.securityWebApi.certificates.CertificateGenerator;
import com.securityWebApi.keystores.KeyStoreReader;
import com.securityWebApi.keystores.KeyStoreWriter;
import com.securityWebApi.model.IssuerBean;
import com.securityWebApi.model.SubjectBean;
import com.securityWebApi.util.Constants;
import com.securityWebApi.util.UtilSecurity;


@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
//		KeyStoreWriter ksw = new KeyStoreWriter();
//		
//		
//		
//		System.out.println("Kreiranje KeyStora...");
//		ksw.loadKeyStore(null, Constants.KEY_STORE_PASSWORD);
//		
//		
//		UtilSecurity us = new UtilSecurity();
//		
//		KeyPair kpAdmin = us.generateKeyPair();
//		IssuerBean issuerData = us.getIssuerData(kpAdmin.getPrivate());
//		
//		CertificateGenerator cg = new CertificateGenerator();
//        X509Certificate cert = cg.generateCertificate(null, issuerData, kpAdmin.getPublic());
//        ksw.write(Constants.ALIAS_SUPERADMIN, kpAdmin.getPrivate(),Constants.PASSWORD_SUPERADMIN,cert);
//        ksw.saveKeyStore(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
//		System.out.println("Kreiran SUPER ADMIN certifikat i upisan u keystore.");

	}

	
}
