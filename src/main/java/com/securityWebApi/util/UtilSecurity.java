package com.securityWebApi.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.CRLException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.xml.bind.DatatypeConverter;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509v2CRLBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CRLConverter;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.io.pem.PemObject;
import org.springframework.stereotype.Service;

import com.securityWebApi.DTO.DTOSubjectInfo;
import com.securityWebApi.certificates.CertificateGenerator;
import com.securityWebApi.keystores.KeyStoreWriter;
import com.securityWebApi.model.IssuerBean;
import com.securityWebApi.model.SubjectBean;

@Service
public class UtilSecurity {
	
	private static final Logger LOG = Logger.getLogger(UtilSecurity.class.getName());

	@PostConstruct
	public void init() {
		LOG.info("Kreiranje KeyStora...");
		createFiles();
			
	}
	
	public void createFiles() {
		try {
			createCer();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

	public KeyPair generateKeyPair() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
            keyGen.initialize(2048, random);
            return keyGen.generateKeyPair();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
    }
	
	public IssuerBean getIssuerData(PrivateKey issuerKey) {
        X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
        builder.addRDN(BCStyle.CN, "Goran Sladic");
        builder.addRDN(BCStyle.SURNAME, "Sladic");
        builder.addRDN(BCStyle.GIVENNAME, "Goran");
        builder.addRDN(BCStyle.O, "UNS-FTN");
        builder.addRDN(BCStyle.OU, "Katedra za informatiku");
        builder.addRDN(BCStyle.C, "RS");
        builder.addRDN(BCStyle.E, "sladicg@uns.ac.rs");

        // UID (USER ID) je ID korisnika
        builder.addRDN(BCStyle.UID, "123456");

        // Kreiraju se podaci za issuer-a, sto u ovom slucaju ukljucuje:
        // - privatni kljuc koji ce se koristiti da potpise sertifikat koji se izdaje
        // - podatke o vlasniku sertifikata koji izdaje nov sertifikat
        return new IssuerBean(builder.build(), issuerKey);
    }
	
	public SubjectBean makeSubjectDataaaa(DTOSubjectInfo dtoSI) {
		try {
            KeyPair keyPairSubject = generateKeyPair();

            // Datumi od kad do kad vazi sertifikat
            SimpleDateFormat iso8601Formater = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = iso8601Formater.parse(dtoSI.getStartDate().toString());
            Date endDate = iso8601Formater.parse(dtoSI.getEndDate().toString());

            // Serijski broj sertifikata
            // PROMENITI, za sad je zakucan
            String sn = "1";

            // klasa X500NameBuilder pravi X500Name objekat koji predstavlja podatke o vlasniku
            X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
            builder.addRDN(BCStyle.CN, "Marija Kovacevic");
            builder.addRDN(BCStyle.SURNAME, "Kovacevic");
            builder.addRDN(BCStyle.GIVENNAME, "Marija");
            builder.addRDN(BCStyle.O, "UNS-FTN");
            builder.addRDN(BCStyle.OU, "Katedra za informatiku");
            builder.addRDN(BCStyle.C, "RS");
            builder.addRDN(BCStyle.E, "marija.kovacevic@uns.ac.rs");

            // UID (USER ID) je ID korisnika
            builder.addRDN(BCStyle.UID, "654321");

            // Kreiraju se podaci za sertifikat, sto ukljucuje:
            // - javni kljuc koji se vezuje za sertifikat
            // - podatke o vlasniku
            // - serijski broj sertifikata
            // - od kada do kada vazi sertifikat
            return new SubjectBean(keyPairSubject.getPublic(), builder.build(), sn, startDate, endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
		
	}

	public SubjectBean genereteSubjectData() {
		KeyPair keyPairSubject = generateKeyPair();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		// Datumi od kad do kad vazi sertifikat
		LocalDateTime startDate = LocalDateTime.now();
		startDate.format(dtf);
		LocalDateTime endDate = startDate.plusYears(1);
		endDate.format(dtf);
		
		Date start = Timestamp.valueOf(startDate);
		Date end = Timestamp.valueOf(endDate);
		// Serijski broj sertifikata
		// PROMENITI, za sad je zakucan
		String sn = "1";

		// klasa X500NameBuilder pravi X500Name objekat koji predstavlja podatke o vlasniku
		X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE);
		builder.addRDN(BCStyle.CN, "Marija Kovacevic");
		builder.addRDN(BCStyle.SURNAME, "Kovacevic");
		builder.addRDN(BCStyle.GIVENNAME, "Marija");
		builder.addRDN(BCStyle.O, "UNS-FTN");
		builder.addRDN(BCStyle.OU, "Katedra za informatiku");
		builder.addRDN(BCStyle.C, "RS");
		builder.addRDN(BCStyle.E, "marija.kovacevic@uns.ac.rs");

		// UID (USER ID) je ID korisnika
		builder.addRDN(BCStyle.UID, "654321");

		return new SubjectBean(keyPairSubject.getPublic(), builder.build(), sn, start, end);
       	
	}
	
	
	private String certToString(X509Certificate cert) {
	    StringWriter sw = new StringWriter();
	    try {
	        sw.write("-----BEGIN CERTIFICATE-----\n");
	        sw.write(DatatypeConverter.printBase64Binary(cert.getEncoded()).replaceAll("(.{64})", "$1\n"));
	        sw.write("\n-----END CERTIFICATE-----\n");
	    } catch (CertificateEncodingException e) {
	        e.printStackTrace();
	    }
	    return sw.toString();
	}
	
	private void createCer() throws OperatorCreationException, CRLException, IOException {
		
		KeyStoreWriter keyStoreWriter = new KeyStoreWriter();
		SubjectBean sBean = genereteSubjectData();
		
		KeyPair keyPair = generateKeyPair();
		sBean.setPublicKey(keyPair.getPublic());
		
		System.out.println("Kreiranje KeyStora...");
		keyStoreWriter.loadKeyStore(null, Constants.KEY_STORE_PASSWORD);
		
		UtilSecurity us = new UtilSecurity();
		
		KeyPair kpAdmin = us.generateKeyPair();
		IssuerBean issuerData = us.getIssuerData(kpAdmin.getPrivate());
		
		
		
		CertificateGenerator cg = new CertificateGenerator();
        X509Certificate cert = cg.generateCertificate(null, issuerData, kpAdmin.getPublic());
        
        
        PemObject po = new PemObject("PRIVATE KEY",kpAdmin.getPrivate().getEncoded());
		
		JcaPEMWriter jpw = new JcaPEMWriter(new FileWriter(new File("Frontend//ssl/server.key")));
		jpw.writeObject(po);
		jpw.flush();
		jpw.close();
        
        FileWriter fw = new FileWriter("Frontend//ssl/server.cer");
        fw.write(certToString(cert));
        fw.close();
        
        
        keyStoreWriter.write(Constants.ALIAS_SUPERADMIN, kpAdmin.getPrivate(),Constants.PASSWORD_SUPERADMIN,cert);
        keyStoreWriter.saveKeyStore(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
        
        createCRL(keyPair.getPrivate(), sBean.getX500name());
		System.out.println("Kreiran SUPER ADMIN certifikat i upisan u keystore.");
	}
	
	private void createCRL(PrivateKey privateKey,X500Name issuerName) throws OperatorCreationException, CRLException, IOException {
		X509v2CRLBuilder builder = new X509v2CRLBuilder(issuerName, new Date());
		builder.setNextUpdate(new Date(System.currentTimeMillis() + 86400 * 1000));
		
		JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder("SHA256WithRSA");
        contentSignerBuilder.setProvider("BC");

        X509CRLHolder crlHolder = builder.build(contentSignerBuilder.build(privateKey));
        JcaX509CRLConverter converter = new JcaX509CRLConverter();
        converter.setProvider("BC");

        X509CRL crl = converter.getCRL(crlHolder);

        byte[] bytes = crl.getEncoded();


        OutputStream os = new FileOutputStream("src/main/resources/certificationInfoList.crl");
        os.write(bytes);
        os.close();	
		
	}
}
