package com.securityWebApi.util;

public class Constants {
	
	public final static String KEY_STORE_NAME = "newKeyStoreFileName.jks";
	public final static String KEY_STORE_PASSWORD = "password";
	
	//KEYSTORE ALIASES and PASSWORDS
	public final static String ALIAS_SUPERADMIN = "SUPERADMIN";
	public final static String PASSWORD_SUPERADMIN = "password";

}
