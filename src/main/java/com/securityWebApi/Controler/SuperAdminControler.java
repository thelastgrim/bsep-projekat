package com.securityWebApi.Controler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.operator.OperatorCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.securityWebApi.DTO.DTOResponseCertificate;
import com.securityWebApi.DTO.DTOSubjectInfo;
import com.securityWebApi.DTO.RevokeCertificateDTO;
import com.securityWebApi.enums.RevokeReason;
import com.securityWebApi.keystores.KeyStoreReader;
import com.securityWebApi.keystores.KeyStoreWriter;
import com.securityWebApi.model.IssuerBean;
import com.securityWebApi.model.SubjectBean;
import com.securityWebApi.service.CertificateService;
import com.securityWebApi.util.Constants;
import com.securityWebApi.util.UtilSecurity;
@CrossOrigin(origins = "https://localhost:4200")
@Controller
@RequestMapping(value = "/certificates")
public class SuperAdminControler {
	
	@Autowired
	private CertificateService certificateService;
	List<String> requests = new ArrayList<String>();
	
	@Autowired
	private UtilSecurity utilSecurity;
	
	private KeyStoreReader ksr = new KeyStoreReader();
	
	private KeyStoreWriter ksw = new KeyStoreWriter();
	
	@CrossOrigin(origins = "https://localhost:4200")
	@PostMapping()
	public ResponseEntity<?> generateCertificate(@RequestBody DTOSubjectInfo dtoSubjectInfo) throws IOException, CertificateEncodingException{
		PrivateKey pkSuperAdmin = ksr.readPrivateKey(Constants.KEY_STORE_NAME,
													 Constants.KEY_STORE_PASSWORD,
													 Constants.ALIAS_SUPERADMIN,
													 Constants.PASSWORD_SUPERADMIN);

		Certificate root = certificateService.getCertificateByName(dtoSubjectInfo.getRoot());
		requests.remove(dtoSubjectInfo.getRequest());
		X509Certificate c_root = (X509Certificate) root;
		
		X500Name x500name = new JcaX509CertificateHolder(c_root).getSubject();


		IssuerBean issuerBean = ksr.readIssuerFromStore(Constants.KEY_STORE_NAME,
														Constants.KEY_STORE_PASSWORD,
														Constants.ALIAS_SUPERADMIN,
														Constants.PASSWORD_SUPERADMIN);

		if(!dtoSubjectInfo.getRoot().equals("Goran Sladic")) {
			IssuerBean rootIssuer = ksr.readIssuerFromStore(Constants.KEY_STORE_NAME,
					Constants.KEY_STORE_PASSWORD,
					dtoSubjectInfo.getRoot(),
					x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString() + x500name.getRDNs(BCStyle.C)[0].getFirst().getValue().toString());
			//System.out.println(rootIssuer);
			issuerBean=rootIssuer;
		}
		KeyPair keyPairSubject = utilSecurity.generateKeyPair();
		SubjectBean subjectBean = certificateService.generateSubjectData(dtoSubjectInfo, keyPairSubject.getPublic());
		X509Certificate cert = certificateService.generateCertificate(issuerBean, subjectBean);
		ksw.loadKeyStore(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
		ksw.write(dtoSubjectInfo.getCertificateName(),
				  keyPairSubject.getPrivate(), dtoSubjectInfo.getCertificateName()+dtoSubjectInfo.getCountry(), 
				  cert);
		ksw.saveKeyStore(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
		System.out.println("Kreiran sertifikat za "+dtoSubjectInfo.getCertificateName());
		
		
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
	

	@RequestMapping(method = RequestMethod.POST,value = "/revoke")
	public ResponseEntity<String> revokeCertificate(@RequestBody RevokeCertificateDTO revokeCerDTO) throws CertificateEncodingException, OperatorCreationException, CRLException{
		try {
			System.out.println("-------------------------");
			System.out.println(revokeCerDTO);
			certificateService.revokeCertificate(revokeCerDTO);
		} catch (IOException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
		
	}

	@GetMapping()
	public ResponseEntity<List<DTOResponseCertificate>> getAllCertificates() throws IOException{
		
		File f = new File("src/main/resources/certificationInfoList.crl");
		byte[] bytes = Files.readAllBytes(f.toPath());
		X509CRLHolder holder = new X509CRLHolder(bytes);
		
		List<DTOResponseCertificate> response = new ArrayList<>();
		List<Certificate> certificates = ksr.readAllCertificates(Constants.KEY_STORE_NAME, Constants.KEY_STORE_PASSWORD);
		
		for (Certificate certificate : certificates) {
			X509Certificate c = (X509Certificate) certificate;
			try {
				X500Name x500name = new JcaX509CertificateHolder(c).getSubject();
				if(!BCStyle.CN.toString().equals("Goran Sladic") && (holder.getRevokedCertificate(c.getSerialNumber()) == null)    ){
					response.add(new DTOResponseCertificate(x500name.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString(), 
						    x500name.getRDNs(BCStyle.SURNAME)[0].getFirst().getValue().toString(), 
						    x500name.getRDNs(BCStyle.GIVENNAME)[0].getFirst().getValue().toString(),
						    c.getNotBefore(),
						    c.getNotAfter()));
				}

			} catch (CertificateEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
		}
		
		return new ResponseEntity<List<DTOResponseCertificate>>(response, HttpStatus.OK);
		
	}

	@RequestMapping(method = RequestMethod.GET,value = "/getRevokeReason")
	public ResponseEntity<List<RevokeReason>> getRevokeReason(){
		try{
			return new ResponseEntity<>(certificateService.getRevokeReason(),HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(method = RequestMethod.POST,value = "/check")
	public ResponseEntity<?> checkCertificateValidity(@RequestBody String alias) throws IOException, CertificateEncodingException{
		
		
		return new ResponseEntity<>(certificateService.checkCertValidity(alias),HttpStatus.OK);

	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/loads")
	public ResponseEntity<List<String>> loadCertificateRequests(){
		if(requests.size()==0) {
			requests.add("Zahtev 1");
			requests.add("Zahtev 2");
			requests.add("Zahtev 3");
			requests.add("Zahtev 4");
			requests.add("Zahtev 5");
			requests.add("Zahtev 6");
		}
		
		
		try{
			return new ResponseEntity<>(requests,HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
 }

